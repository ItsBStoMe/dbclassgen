#tag Class
Protected Class GenDB
	#tag Method, Flags = &h0
		Sub AddStatus(s as string)
		  me.GenStatus.Append s
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearReadMe()
		  For i as integer = 0 to GenStatus.Ubound
		    GenStatus.Remove(i)
		  Next 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  Dim d as new Date 
		  
		  AddStatus("DBClass Generator")
		  AddStatus("Creation Date: " + d.LongDate )
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetClassHeader() As Text
		  Dim rtnText as string
		  
		  rtnText = "#tag Class" + EndOfLine _
		  + "Protected Class " + Name + EndOfLine _
		  + "Inherits " + Type + EndOfLine
		  
		  AddStatus("Database Type: " + Type)
		  
		  return rtnText.ToText
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetColumns(ByRef Table as dbTable)
		  Dim rs  as RecordSet
		  
		  Select Case me.Type
		    
		  Case "SQLiteDatabase"
		    rs = SQLiteDB.FieldSchema(Table.name)
		  Case "MySQLCommunityServer"
		    rs = MySQLDB.FieldSchema(Table.name)
		  Case "MSSQLServerDatabase"
		    rs = MSSQLDB.FieldSchema(Table.name)
		  Case "OracleDatabase"
		    rs = OracleDB.FieldSchema(Table.name)
		  Case "PostgreSQLDatabase"
		    rs = PostgreSQLDB.FieldSchema(Table.name)
		  End Select
		  
		  While NOT rs.eof
		    Dim col as new dbColumn
		    col.name = rs.IdxField(1).StringValue
		    col.datatype = rs.IdxField(2)
		    Table.columns.Append col
		    
		    rs.movenext
		  Wend
		  rs.close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetInsertMethods(showHeader as boolean) As String
		  Dim rtnText as Text
		  
		  AddStatus(EndofLine + "*** Insert Functions ***")
		  
		  If showHeader Then 
		    //GetCommentHeader
		  End If 
		  
		  for i as integer = 0 to dbTables.Ubound
		    if dbTables(i).name <> "sqlite_sequence" Then 
		      rtnText = GetInserts(dbTables(i).name).ToText
		    End If
		  next
		  
		  return rtnText
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetInserts(TableName as String) As String
		  Dim output as String
		  Dim columnNames, valueName as String
		  columnNames = " ("
		  valueName = "Values("
		  
		  for tid as integer = 0 to me.dbTables.Ubound
		    If TableName = dbTables(tid).name Then
		      for cid as integer = 0 to dbTables(tid).columns.Ubound
		        columnNames = columnNames +  dbTables(tid).columns(cid).name + ", "
		        valueName = valueName + "p" + dbTables(tid).columns(cid).name + ", "
		      Next
		    End If
		  Next
		  
		  // *BS* If Id is auto incrementing, don't include
		  //
		  
		  // Clean up extra commas add close parenthesis
		  If columnNames.Right(2) = ", " Then 
		    columnNames = mid(columnNames, 1, columnNames.Len - 2)  + ")"
		  End If 
		  
		  If valueName.Right(2) = ", " Then 
		    valueName = mid(valueName, 1, valueName.Len - 2) + ")"
		  End If 
		  
		  output = " #tag Method, Flags = &h0" + EndOfLine _
		  + "  Function Add"+ TableName + "() As RecordSet" + endofline _
		  + "  INSERT into " + TableName  + columnNames  + EndOfLine  _
		  +  valueName + EndOfLine _
		  + "   return rs" + EndOfLine _
		  + "  End Function" + EndOfLine _
		  + " #tag EndMethod" + EndofLine + EndOfLine
		  
		  AddStatus("Add" +  TableName + "()  returns RecordSet.")
		  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSelectMethods(showHeader as boolean) As text
		  Dim rtnText as Text
		  
		  Select Case Type 
		    
		  Case "SQLiteDatabase"
		    If Not SQLiteDB.Connect then 
		      msgbox("Unable to connect to SQLite Database.")
		      rtnText = "Error"
		    End If 
		    AddStatus(EndofLine + "*** Select Functions ***")
		    
		    'If SQLiteDB.Error then
		    'MsgBox("Error: " + SQLiteDB.ErrorMessage)
		    'Else
		    If showHeader Then 
		      //GetCommentHeader
		    End If 
		    'End If
		    for i as integer = 0 to dbTables.Ubound
		      if dbTables(i).name <> "sqlite_sequence" Then 
		        rtnText = GetSelects(dbTables(i).name).ToText
		      End If
		    next
		    
		  Case "MySQLCommunityServer"
		    If Not MySQLDB.Connect  then 
		      msgbox("Unable to connect to MySQL Database.")
		      rtnText = "ERROR" 
		    End If 
		    AddStatus(EndofLine + "*** Functions ***")
		    
		    'If MySQLDB.Error then 
		    'MsgBox("Error: " + MySQLDB.ErrorMessage)
		    'Else
		    If showHeader Then 
		      //GetCommentHeader
		    End If 
		    
		    'for i as integer = 0 to dbTables.Ubound
		    'TextArea1.AppendText(GetSelects(dbTables(i).name).ToText)
		    'next
		    
		    'End If
		  End Select
		  
		  return rtnText
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetSelects(TableName as String) As String
		  Dim output as String
		  
		  output = " #tag Method, Flags = &h0" + EndOfLine _
		  + "  Function Get"+ TableName + "() As RecordSet" + endofline _
		  + "    Dim rs as recordset = App.DB.SqlSelect(""Select * from " + TableName + """)" _
		  + EndOfLine + EndOfLine _
		  + "   return rs" + EndOfLine _
		  + "  End Function" + EndOfLine _
		  + " #tag EndMethod" + EndofLine + EndOfLine
		  
		  AddStatus("Get" +  TableName + "()  returns RecordSet.")
		  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub getTables()
		  // Add Connect to DB
		  
		  Dim rs  as RecordSet
		  
		  Select Case me.Type
		  Case "SQLiteDatabase"
		    If SQLiteDB.Connect Then
		      rs = SQLiteDB.TableSchema
		    End If 
		  Case "MySQLCommunityServer"
		    rs = MySQLDB.TableSchema
		  Case "MSSQLServerDatabase"
		    rs = MSSQLDB.TableSchema
		  Case "OracleDatabase"
		    rs = OracleDB.TableSchema
		  Case "PostgreSQLDatabase"
		    rs = PostgreSQLDB.TableSchema
		  End Select
		  
		  while NOT rs.eof
		    Dim t as new dbTable
		    
		    t.name = rs.IdxField(1).StringValue
		    dbTables.Append t
		    GetColumns(t)
		    
		    rs.movenext
		  wend
		  
		  rs.close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetUpdates(TableName as String) As String
		  Dim output as String
		  Dim columnNames, valueName as String
		  columnNames = " ("
		  valueName = "Values("
		  
		  for tid as integer = 0 to me.dbTables.Ubound
		    If TableName = dbTables(tid).name Then
		      for cid as integer = 0 to dbTables(tid).columns.Ubound
		        columnNames = columnNames +  dbTables(tid).columns(cid).name + ", "
		      Next
		    End If
		  Next
		  
		  // *BS* If Id is auto incrementing, don't include
		  //
		  
		  // Clean up extra commas add close parenthesis
		  If columnNames.Right(2) = ", " Then 
		    columnNames = mid(columnNames, 1, columnNames.Len - 2)  + ")"
		  End If 
		  
		  If valueName.Right(2) = ", " Then 
		    valueName = mid(valueName, 1, valueName.Len - 2) + ")"
		  End If 
		  
		  output = " #tag Method, Flags = &h0" + EndOfLine _
		  + "  Function Update "+ TableName + endofline _
		  + "  Update " + TableName  + EndOfLine  _
		  + "   Set  + EndOfLine _
		  + "   return rs" + EndOfLine _
		  + "  End Function" + EndOfLine _
		  + " #tag EndMethod" + EndofLine + EndOfLine
		  
		  AddStatus("Add" +  TableName + "()  returns RecordSet.")
		  
		  return output
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		dbColumns() As dbColumn
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mDBFile
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mDBFile = value
			End Set
		#tag EndSetter
		DBFile As FolderItem
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		dbTables() As dbTable
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mEncrypted
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mEncrypted = value
			End Set
		#tag EndSetter
		Encrypted As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		GenStatus() As String
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mHost
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mHost = value
			End Set
		#tag EndSetter
		Host As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mKeyValue
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mKeyValue = value
			End Set
		#tag EndSetter
		KeyValue As String
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private mDBFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mEncrypted As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mHost As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mKeyValue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mName As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPassword As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mPort As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		MSSQLDB As MSSQLServerDatabase
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mType As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mUsername As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MySQLDB As MySQLCommunityServer
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mName
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mName = value
			End Set
		#tag EndSetter
		Name As String
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		OracleDB As OracleDatabase
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPassword
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPassword = value
			End Set
		#tag EndSetter
		Password As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mPort
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mPort = value
			End Set
		#tag EndSetter
		Port As Integer
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		PostgreSQLDB As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		SQLiteDB As SQLiteDatabase
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mType
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mType = value
			End Set
		#tag EndSetter
		Type As String
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mUsername
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  mUsername = value
			End Set
		#tag EndSetter
		Username As String
	#tag EndComputedProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Encrypted"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Host"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="KeyValue"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Password"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Port"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Type"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Username"
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
