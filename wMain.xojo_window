#tag Window
Begin Window wMain
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   570
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   570
   MaximizeButton  =   True
   MaxWidth        =   840
   MenuBar         =   914501631
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "DB Class Generator"
   Visible         =   True
   Width           =   840
   Begin TabPanel TabPanel1
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   166
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "Data\rErrors\rInfo"
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   203
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   286
      Begin bsCheckBox cbUpdateMethods
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Update Methods"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Update Methods : Creates Update methods based on recrod Id passed."
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   32
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   269
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   129
      End
      Begin bsCheckBox cbGetMethods
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Get Methods"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Get Methods : Create select (get) for all columns in each table in database"
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   32
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   246
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   129
      End
      Begin bsCheckBox cbDeleteMethods
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Delete Methods"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Delete method : Creates Delete method based on record ID"
         HelpTag         =   "Delete method : Creates Delete method based on record ID"
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   167
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   3
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   269
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   122
      End
      Begin bsCheckBox cbAddMethods
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Add Methods"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Add Methods : Create 'Insert' method for each table in database."
         HelpTag         =   "Add Methods : Create 'Insert' method for each table in database."
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   167
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   2
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   246
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   122
      End
      Begin bsCheckBox cbErrorMessages
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Display DB Error Message to user"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Includes Error # and messages in messagebox to user"
         HelpTag         =   "Includes Error # and messages in messagebox to user"
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   269
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   246
      End
      Begin bsCheckBox cbErrorToFile
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Write errors to filesytem"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Write Errors to filesystem"
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   246
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   246
      End
      Begin bsCheckBox cbErrorMessages1
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Display DB Error # to user"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Includes Error # and messages in messagebox to user"
         HelpTag         =   "Includes Error # and messages in messagebox to user"
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   2
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   291
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   246
      End
      Begin bsCheckBox cbReadMe
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Include ReadMe with class"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Provides ReadMe within database class being generated."
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   246
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   246
      End
      Begin bsCheckBox cbMethodHeaders
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Include header comment block"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Method Headers : Includes formatted method header comment block.\n//*****************************\n// Name: {method_name} \n// Purpose: Select * from {table} \n// Created: {create_date}\n//*****************************\n"
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   269
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   246
      End
      Begin bsCheckBox cbGetProcedures
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpMessage     =   "Update Methods : Creates Update methods based on recrod Id passed."
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "TabPanel1"
         Italic          =   False
         Left            =   32
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   4
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   293
         Underline       =   False
         Value           =   False
         Visible         =   False
         Width           =   139
      End
   End
   Begin PushButton btnExit
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Exit"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   753
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   540
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin TextArea TextArea1
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   536
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      Italic          =   False
      Left            =   318
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   True
      ScrollbarVertical=   True
      Styled          =   True
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   522
   End
   Begin PushButton btnGenerate
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Generate"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   216
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   381
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin PopupMenu popUpDBType
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   126
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   12
      Underline       =   False
      Visible         =   True
      Width           =   122
   End
   Begin PagePanel ConnectionPanel
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   165
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PanelCount      =   3
      Panels          =   ""
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      Top             =   40
      Value           =   1
      Visible         =   True
      Width           =   286
      Begin TextField txtFilename
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   104
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   51
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   167
      End
      Begin TextField txtHostname
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   148
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   51
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   141
      End
      Begin TextField txtPort
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   148
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   81
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   47
      End
      Begin TextField txtSchema
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   148
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   110
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   141
      End
      Begin TextField txtPassword
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   148
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   True
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   166
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   141
      End
      Begin TextField txtUsername
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   148
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   138
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   141
      End
      Begin Label Label2
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   72
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   3
         Text            =   "File"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   52
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   27
      End
      Begin PushButton btnSelectFile
         AutoDeactivate  =   True
         Bold            =   False
         ButtonStyle     =   "0"
         Cancel          =   False
         Caption         =   "..."
         Default         =   False
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   272
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   51
         Underline       =   False
         Visible         =   True
         Width           =   27
      End
      Begin CheckBox cbEnctrypted
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Encypted"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   104
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   3
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   78
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   84
      End
      Begin Label lblKey
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   70
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   3
         Text            =   "Key"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   102
         Transparent     =   True
         Underline       =   False
         Visible         =   False
         Width           =   32
      End
      Begin TextField txtKey
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   106
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   3
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   101
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   False
         Width           =   141
      End
      Begin Label Label3
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   2
         Text            =   "Hostname"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   52
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   96
      End
      Begin Label Label4
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   59
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   2
         Text            =   "Port"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   82
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   77
      End
      Begin Label Label5
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   2
         Text            =   "Default Schema"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   111
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   96
      End
      Begin Label Label6
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   2
         Text            =   "Username"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   139
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   96
      End
      Begin Label Label7
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ConnectionPanel"
         Italic          =   False
         Left            =   40
         LockBottom      =   False
         LockedInPosition=   True
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   2
         Text            =   "Password"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   166
         Transparent     =   True
         Underline       =   False
         Visible         =   True
         Width           =   96
      End
   End
   Begin Label Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      Text            =   "Database Type"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   11
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   95
   End
   Begin Label lblInfo
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   141
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      Text            =   " "
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   419
      Transparent     =   True
      Underline       =   False
      Visible         =   True
      Width           =   286
   End
   Begin PushButton btnSave
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Save"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   318
      LockBottom      =   False
      LockedInPosition=   True
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   540
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event , Description = 4F70656E2057696E646F7720616E6420696E7374616E74696174652076617269616C626573
		Sub Open()
		  'DB = new GenDB
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub DisplayInfo(stringValue as String)
		  lblinfo.Text = stringValue
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GenerateFile()
		  DB = new GenDB
		  
		  Select Case popUpDBType.ListIndex
		  Case 5
		    DB.Type = "SQLiteDatabase"
		    // Ensure DB File has been selected.
		    If txtFilename.text = "" Then 
		      msgbox "Database file required  to proceeding."
		      return
		    End If
		    
		    DB.SQLiteDB = New SQLiteDatabase
		    DB.SQLiteDB.DatabaseFile = Filename
		    DB.Name = mid(DB.SQLiteDB.DatabaseFile.Name, 1, instr(DB.SQLiteDB.DatabaseFile.Name, ".")-1) 
		    DB.getTables
		    
		  Case 1
		    DB.Type = "MSSQLServerDatabase"
		    
		  Case 2
		    DB.Type = "MySQLCommunityServer"
		    DB.MySQLDB = New MySQLCommunityServer
		    DB.MySQLDB.Host = txtHostname.text
		    DB.MySQLDB.port = txtPort.Text.Val
		    DB.MySQLDB.DatabaseName = txtSchema.text
		    DB.MySQLDB.UserName = txtUsername.text
		    DB.MySQLDB.Password = txtPassword.text
		    DB.getTables
		    
		  Case 3
		    DB.Type = "OracleDatabase"
		    
		  Case 4
		    DB.Type = "PostgreSQLDatabase"
		    
		  Case Else
		    msgbox "You must select a database type."
		    return
		  End Select
		  
		  // Add Class Header
		  TextArea1.AppendText(DB.GetClassHeader())
		  
		  // Get Select methods for each table in Database
		  if cbGetMethods.Value then
		    textarea1.appendText(db.GetSelectMethods(cbMethodHeaders.Value))
		  end if
		  
		  
		  // Get Insert methods for each table in Database
		  If cbAddMethods.Value Then
		    textarea1.appendText(db.GetInsertMethods(cbMethodHeaders.Value))
		  End If 
		  
		  // Get Update methods for each table in Database
		  'If cbUpdateMethods.Value Then
		  'textarea1.appendText(db.GetIUpdateMethods(cbMethodHeaders.Value))
		  'End If 
		  
		  // Get Delete methods for each table in Database
		  'If cbDeleteMethods.Value Then
		  'textarea1.appendText(db.GetIDeleteMethods(cbMethodHeaders.Value))
		  'End If 
		  
		  // Get DBOpen Block
		  textarea1.appendText(GetOpen(DB.Type).ToText)
		  
		  // Get ReadMe block
		  If cbReadMe.State = CheckBox.CheckedStates.Checked Then
		    Textarea1.AppendText(GetReadMe().ToText)
		  End If
		  
		  // Get Properties Block
		  textarea1.appendText(GetProperties(DB.Type).ToText)
		  
		  // Get Behaviors Block
		  textarea1.appendText(GetBehavior().ToText)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetBehavior() As String
		  Dim output as String
		  
		  output = "#tag ViewBehavior" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""DatabaseFile""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""FolderItem""" + EndOfLine _
		  + "   EditorType=""FolderItem""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""DebugMode""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Boolean""" + EndOfLine _
		  + "   EditorType=""Boolean""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""EncryptionKey""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""String""" + EndOfLine _
		  + "   EditorType=""String""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""filename""" + EndOfLine _
		  + "   Group=""Behavior""" + EndOfLine _
		  + "   Type=""String""" + EndOfLine _
		  + "   EditorType=""MultiLineEditor""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Index""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Group=""ID""" + EndOfLine _
		  + "   Type=""Integer""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Left""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Group=""Position""" + EndOfLine _
		  + "   Type=""Integer""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""LoadExtensions""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Boolean""" + EndOfLine _
		  + "   EditorType=""Boolean""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""MultiUser""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Boolean""" + EndOfLine _
		  + "   EditorType=""Boolean""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Name""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Group=""ID""" + EndOfLine _
		  + "   Type=""String""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""ShortColumnNames""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Boolean""" + EndOfLine _
		  + "   EditorType=""Boolean""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Super""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Group=""ID""" + EndOfLine _
		  + "   Type=""String""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""ThreadYieldInterval""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Integer""" + EndOfLine _
		  + "   EditorType=""Integer""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Timeout""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Type=""Double""" + EndOfLine _
		  + "   EditorType=""Double""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + "  #tag ViewProperty" + EndOfLine _
		  + "   Name=""Top""" + EndOfLine _
		  + "   Visible=true" + EndOfLine _
		  + "   Group=""Position""" + EndOfLine _
		  + "   Type=""Integer""" + EndOfLine _
		  + "  #tag EndViewProperty" + EndOfLine _
		  + " #tag EndViewBehavior" + EndOfLine _
		  + "End Class" + EndOfLine _
		  + "#tag EndClass"  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetCommentHeader(name as string, purpose as string, inout as string) As String
		  Dim output as String
		  Dim d as new Date
		  
		  output = "//********************************" + EndOfLine _
		  + "  // Name:" + name + EndOfLine _
		  + "  // Purpose:" + purpose + EndOfLine _
		  + "  // Input/Output:" + inout + EndOfLine _
		  + "  // Created: " +  d.SQLDateTime + EndOfLine _
		  + "  //********************************" + endofline
		  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetOpen(dbType as String) As String
		  Dim output, sStatus as String
		  
		  Select Case dbType 
		  Case "SQLiteDatabase"
		    output = " #tag Method, Flags = &h0" + EndOfLine _
		    + "  Sub OpenDB()" + EndOfLine _
		    + "   me.DatabaseFile = GetFolderItem(filename)" + EndOfLine _
		    + "   If not me.Connect then " + EndOfLine _
		    + "    msgbox ""Error :"" + me.ErrorMessage" + EndOfLine _ 
		    + "   End If" + EndOfLine _
		    + "  End Sub" + EndOfLine _
		    + " #tag EndMethod" + EndOfLine + EndOfLine
		    
		    sStatus = EndOfLine + "*** Instantiate DBClass ***"  + EndOfLine _
		    + " 1. Add db property of type " + db.Type + endofline _
		    + "  2. Add the following to the App/Session Open Event:" + EndOfLine _
		    + "          db = new {name of DBClass}" + EndOfLine _
		    + "          App.db.OpenDB   " + EndOfLine _
		    + "  3. Add App.db.close to App.Close event." 
		    
		  Case "MySQLCommunityServer"
		    output = " #tag Method, Flags = &h0" + EndOfLine _
		    + "  Sub OpenDB()" + EndOfLine _
		    + "   If not me.Connect then " + EndOfLine _
		    + "    msgbox ""Error :"" + me.ErrorMessage" + EndOfLine _ 
		    + "   End If" + EndOfLine _
		    + "  End Sub" + EndOfLine _
		    + " #tag EndMethod" + EndOfLine + EndOfLine
		    
		    sStatus = EndOfLine + "*** Instantiate DBClass ***"  + EndOfLine _
		    + " 1. Add db property of type " + db.Type + endofline _
		    + "  2. Add the following to the App/Session Open Event:" + EndOfLine _
		    + "db = new {name of DBClass}" + EndOfLine _
		    + "db.Host = """ + DB.MySQLDB.Host  + """" + EndOfLine _
		    + "db.Port = " + Str(DB.Mysqldb.port) + EndOfLine _
		    + "db.DatabaseName = """ + DB.MySQLDB.DatabaseName  + """" +  EndOfLine _
		    + "db.Username = """ + DB.MySQLDB.Username + """" + EndOfLine _
		    + "db.Password = """ + DB.MySQLDB.Password + """" + EndOfLine _
		    + "  3. Add App.db.close to App.Close event." 
		  End Select
		  
		  db.AddStatus(sStatus)
		  
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetProperties(dbType as String) As String
		  Dim output as String
		  
		  Select Case dbType
		  Case "SQLiteDatabase"
		    output = "#tag Property, Flags = &h0" + endofline _
		    + " filename As String = """ + DB.SQLiteDB.DatabaseFile.Name + """" + endofline _
		    + "#tag EndProperty" + EndOfLine + EndOfLine
		    DB.AddStatus("Do not forget: Add filename property of type String to App.")
		    
		  Case Else 
		    output = "#tag Property, Flags = &h0" + endofline _
		    + "#tag EndProperty" + EndOfLine + EndOfLine
		    
		  End Select
		  return output
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetReadMe() As String
		  Dim output as String
		  
		  output = " #tag Note, Name = ReadMe" + endofline + endofline 
		  For i as integer = 0 to DB.GenStatus.Ubound
		    output = output + DB.GenStatus(i) + endofline 
		  Next
		  
		  output = output + "#tag EndNote" + endofline + endofline
		  return output
		  
		  
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		DB As GenDB
	#tag EndProperty

	#tag Property, Flags = &h0
		filename As FolderItem
	#tag EndProperty


#tag EndWindowCode

#tag Events cbUpdateMethods
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbGetMethods
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbDeleteMethods
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbAddMethods
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbErrorMessages
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbErrorToFile
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbErrorMessages1
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbReadMe
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbMethodHeaders
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbGetProcedures
	#tag Event
		Sub MouseEnter()
		  DisplayInfo(me.HelpMessage)
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  DisplayInfo("")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnExit
	#tag Event
		Sub Action()
		  quit
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnGenerate
	#tag Event
		Sub Action()
		  TextArea1.Text = ""
		  GenerateFile()
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events popUpDBType
	#tag Event
		Sub Open()
		  me.addrow("Select ...")
		  me.addrow("MSSQLServer")
		  me.addrow("MySQL")
		  me.addrow("Oracle")
		  me.addrow("PostgreSQL")
		  me.addrow("SQLite")
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  'me.addrow("Select ...")
		  'me.addrow("MSSQLServer")
		  'me.addrow("MySQL")
		  'me.addrow("Oracle")
		  'me.addrow("PostgreSQL")
		  'me.addrow("SQLite")
		  
		  Select Case me.ListIndex
		  Case 1
		    Connectionpanel.Value = 1
		  Case 2
		    Connectionpanel.Value = 1
		    txtPort.Text= "3306"
		    txtHostname.text = "localhost"
		    txtUsername.text = "nodeuser"
		    txtPassword.text = "usernode"
		  Case 3
		    Connectionpanel.Value = 1
		    txtPort.text = "1521"
		  Case 4
		    Connectionpanel.Value = 1
		    txtPort.text = "5432"
		  Case 5
		    Connectionpanel.Value = 2
		  Case Else
		    Connectionpanel.Value = 0
		  End Select
		  
		  Connectionpanel.Visible = true
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ConnectionPanel
	#tag Event
		Sub Open()
		  me.PanelIndex = 0
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSelectFile
	#tag Event
		Sub Action()
		  filename = GetOpenFolderItem("")
		  txtFilename.text = filename.Name
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbEnctrypted
	#tag Event
		Sub Action()
		  if me.value then
		    lblKey.visible = true
		    txtkey.Visible = true
		  else
		    lblKey.Visible = false
		    txtKey.Visible = false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnSave
	#tag Event
		Sub Action()
		  if textarea1.Text <> "" Then
		    Dim dlg as New SaveAsDialog
		    Dim f as FolderItem
		    dlg.InitialDirectory=SpecialFolder.Documents
		    dlg.promptText="Save your newly created Xojo Database Class File"
		    dlg.SuggestedFileName=DB.Name
		    dlg.Title="Save DBClass File"
		    dlg.Filter=FileTypes1.XojoCode  //defined as a file type in FileTypes1 file type set
		    f=dlg.ShowModal()
		    
		    'If f <> Nil then
		    'Dim fileStream As TextOutputStream
		    'fileStream = TextOutputStream.Create(file)
		    'fileStream.WriteLine(NameField.Text)
		    'fileStream.WriteLine(AddressField.Text)
		    'fileStream.WriteLine(PhoneField.Text)
		    'fileStream.Close
		  Else
		    //user canceled
		  End if
		  'End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
